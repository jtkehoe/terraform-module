resource "aws_security_group" "ecs_sg" {
  count = var.is_ecs ? 1 : 0
  egress = [
    {
      cidr_blocks = [
        "0.0.0.0/0",
      ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = false
      to_port          = 0
    },
  ]
  ingress = [
    {
      cidr_blocks = [
        "0.0.0.0/0",
      ]
      description      = ""
      from_port        = 3000
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "tcp"
      security_groups  = []
      self             = false
      to_port          = 3000
    }
  ]
  name   = "${var.name}-ecs-${var.environment}"
  vpc_id = "123vpc"
  tags = {
    App         = var.name
    Environment = var.environment
    Owner       = "aa-eng"
    Name        = "${var.name}-ecs-${var.environment}"
  }
}
