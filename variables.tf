variable "name" {}
variable "environment" {
  validation {
    condition = (
      var.environment == "test" || var.environment == "prod"
    )
    error_message = "The environment variable must be either test or prod."
  }
}
